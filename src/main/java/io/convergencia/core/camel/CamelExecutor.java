package io.convergencia.core.camel;
import java.io.UnsupportedEncodingException;

import org.apache.camel.spring.Main;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import convergencia.api.Log;
import io.convergencia.core.entity.ConvergenciaIOException;
import io.convergencia.core.entity.Route;


public class CamelExecutor {

    private String name;

    private Main main;

    private Route route;

    private ConfigurableApplicationContext applicationContext;

    private boolean running;

    private CamelExecutor(Route route) throws UnsupportedEncodingException {
        this.route = route;
        this.name = route.getName();

        Resource resource = new ByteArrayResource(route.getXmlCode().getBytes("utf-8"));
        GenericXmlApplicationContext springContext = new GenericXmlApplicationContext();
        this.applicationContext = springContext;
        springContext.load(resource);


        ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) applicationContext).getBeanFactory();
        beanFactory.registerSingleton("route", route);


        springContext.refresh();

        this.main = new Main();
        main.setApplicationContext((AbstractApplicationContext) springContext);
    }

    public static CamelExecutor getInstance(Route route) throws ConvergenciaIOException{
        try {
            return new CamelExecutor(route);
        } catch (UnsupportedEncodingException e) {
            Log.error(e.getMessage(), e);
            throw new ConvergenciaIOException(e.getMessage(), e);
        }
    }

    public synchronized void start(){
        new Thread(){
            public void run() {
                try {
                    main.run();
                    running = true;
                } catch (Throwable e) {
                    Log.error(e.getMessage(), e);
                    stopExecutor();
                }
            }
        }.start();
    }

    public synchronized void stopExecutor(){
        running = false;
        try {
            main.completed();
            applicationContext.close();
        } catch (Throwable e) {
            Log.error(e.getMessage(), e);
        }

    }

    public String getName() {
        return name;
    }

    public Main getMain() {
        return main;
    }

    public Route getRoute() {
        return route;
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((route == null) ? 0 : route.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CamelExecutor)) {
            return false;
        }
        CamelExecutor other = (CamelExecutor) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (route == null) {
            if (other.route != null) {
                return false;
            }
        } else if (!route.equals(other.route)) {
            return false;
        }
        return true;
    }

}