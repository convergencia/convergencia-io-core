package io.convergencia.core.camel;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import io.convergencia.core.Message;
import io.convergencia.core.Response;

public class SendMessageProcessor implements Processor{

    @Override
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        try {
            Map<String, Object> bodyMap = mapper.readValue(body, Map.class);
            Map<String, Object> headers = new HashMap<>();
            
            for(Entry<String, Object> entry : exchange.getIn().getHeaders().entrySet()){
                if(entry.getValue() != null){
                    headers.put(entry.getKey(), entry.getValue().toString());
                }
            }
            
            String queue = (String) headers.get("queue");
            if(queue != null){
                Message message = new Message();
                message.setBody(bodyMap);
                message.setHeaders(headers);
                message.setGroup((String) headers.get("group"));
                String uuid = sendJms(queue, message);
                Response resp = new Response(uuid, "SUCCESS", "");
                exchange.getOut().setBody(mapper.writeValueAsString(resp));
            }else{
                Response resp = new Response("", "ERROR", "Queue don't informed");
                exchange.getOut().setBody(mapper.writeValueAsString(resp));
            }
            
        } catch (JMSException e) {
            e.printStackTrace();
            Response resp = new Response("", "ERROR", "JMS Error");
            exchange.getOut().setBody(mapper.writeValueAsString(resp));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            Response resp = new Response("", "ERROR", "Invalid Json");
            exchange.getOut().setBody(mapper.writeValueAsString(resp));
        }

    }

    public String sendJms(String queue, Message messageObj) throws JMSException {
        try {

            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();
            String uuid = UUID.randomUUID().toString();
            // Create a Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue(queue);

            // Create a MessageProducer from the Session to the Topic or
            // Queue
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            // Create a messages
            ObjectMapper mapper = new ObjectMapper();
            messageObj.setId(uuid);
            String json = mapper.writeValueAsString(messageObj);

            TextMessage message = session.createTextMessage(json);
            message.setJMSCorrelationID(uuid);

            // Tell the producer to send the message
            //                    System.out.println("Sent message: " + text);
            producer.send(message);

            // Clean up
            session.close();
            connection.close();
            return uuid;
        } catch (JMSException e) {
            throw e;
        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
            return null;
        }
    }
}
