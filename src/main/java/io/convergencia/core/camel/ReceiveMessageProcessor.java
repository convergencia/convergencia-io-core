package io.convergencia.core.camel;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import convergencia.api.Log;
import io.convergencia.core.Response;

public class ReceiveMessageProcessor implements Processor{

    private ExceptionListenerImpl exceptionListener = new ExceptionListenerImpl();
    
    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        try {
            String queue = (String) exchange.getIn().getHeader("queue");
            if(queue != null){
                String messageOut = receiveJms(queue);
                exchange.getOut().setBody(messageOut);
            }else{
                Response resp = new Response("", "ERROR", "Queue don't informed");
                exchange.getOut().setBody(mapper.writeValueAsString(resp));
            }
        } catch (JMSException e) {
            e.printStackTrace();
            Response resp = new Response("", "ERROR", "JMS Error");
            exchange.getOut().setBody(mapper.writeValueAsString(resp));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            Response resp = new Response("", "ERROR", "Invalid Json");
            exchange.getOut().setBody(mapper.writeValueAsString(resp));
        }

    }

    public String receiveJms(String queue) throws JMSException {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

            Connection connection = connectionFactory.createConnection();
            connection.start();

            connection.setExceptionListener(exceptionListener);

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue(queue);

            MessageConsumer consumer = session.createConsumer(destination);

            javax.jms.Message message = consumer.receive(1000);
            
            try{
                
                if (message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    String text = textMessage.getText();
                    return text;
                } else {
                    return message.toString();
                }
            }finally{
                
                consumer.close();
                session.close();
                connection.close();
            }

        } catch (JMSException e) {
            throw e;
        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
            return null;
        }
    }
    
    private class ExceptionListenerImpl implements javax.jms.ExceptionListener{

        @Override
        public void onException(JMSException exception) {
            Log.error(exception.getMessage(), exception);
        }

    }
}
