package io.convergencia.core;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;

import io.convergencia.core.camel.CamelExecutor;
import io.convergencia.core.entity.Route;

public class App {
    public static void main(String[] args) throws Exception {
        File routeDir = new File("routes");
        for(File file : routeDir.listFiles()){
            FileInputStream fis = new FileInputStream(file);
            String content = IOUtils.toString(fis, "utf-8");
            Route route = new Route();
            route.setName(file.getName());
            route.setXmlCode(content);
            CamelExecutor.getInstance(route).start();
        }
    }
}
