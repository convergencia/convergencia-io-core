package io.convergencia.core.entity;

public class Route {

    private String name;

    private String xmlCode;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXmlCode() {
        return xmlCode;
    }

    public void setXmlCode(String xmlCode) {
        this.xmlCode = xmlCode;
    }
    
}
