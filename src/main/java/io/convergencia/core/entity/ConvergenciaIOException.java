package io.convergencia.core.entity;

public class ConvergenciaIOException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ConvergenciaIOException() {
        super();
    }

    public ConvergenciaIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConvergenciaIOException(String message) {
        super(message);
    }

    public ConvergenciaIOException(Throwable cause) {
        super(cause);
    }
}
